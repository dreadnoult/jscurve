var path = new Path();
var handle1, handle2;
var currentSegment;
var hitResult;
var isInitHandlesCreated = false;
path.name = "pathCurve";
path.strokeColor = 'black';
path.strokeWidth = 4;
path.fullySelected = true;
var cellPath;
var cellsArray;
var xc, yc;
var offset = 200;
var cellCenter = new Point();
var cellText;


// for (var i = 0; i <= 7; i++) {
//   for (var j = 0; j <= 6; j++) {
//     xc = 40 * 3/2 * j;
//     yc = 40 * Math.sqrt(3)*(i+j%2)-20*Math.sqrt(3)*(j%2);
//     cellCenter.x = xc+offset;
//     cellCenter.y = yc+offset;
//     var cellPath = new Path.RegularPolygon(cellCenter, 6, 40);
//     cellText = new PointText(cellCenter);
//     cellText.fillColor = 'black';
//     cellText.content = "i:" + i + " j:"+j; 
//     cellPath.rotate(30, cellCenter);
//     cellPath.strokeColor = '#008CBA';
//     cellPath.strokeWidth = 4;
// }
// }


function onMouseDown(event) {
  // Add a segment to the path at the position of the mouse:
  console.log(typeof path);
  if (mode === "path") {
    if (path == null) {
      path = new Path();
      path.strokeColor = 'black';
      path.strokeWidth = 4;
      console.log("here");
    }
    if (path.segments.length < 2) {
      path.add(event.point);
      path.fullySelected = true;
    } else {
      // Do a hit test on path for handles and segments:
      hitResult = path.hitTest(event.point, { segments: true, handles: true, tolerance: 10 });
    }
    if (path.segments.length === 1){
      isInitHandlesCreated = false;
    }
    if ((path.segments.length === 2) && (!isInitHandlesCreated)) {
      createInitHandles();
       
    }
  } 
  else {
    var cellPath = new Path.RegularPolygon(event.point, 6, 40);
    cellPath.strokeColor = '#008CBA';
    cellPath.strokeWidth = 4;
  }
  
}

path.onDoubleClick = function(event) {
  if (mode === "path") {
    hitResult = path.hitTest(event.point, { curves: true, tolerance: 20 });
    if (hitResult) {
      var newSegment = path.divideAt(hitResult.location);
      path.fullySelected = true;
      console.log(hitResult);
    }
  }
  else {

  }
}

function onMouseDrag(event) {
  if (mode === "path") {
    if (hitResult) {
      currentSegment = hitResult.segment;
      if (hitResult.type === "handle-in") {
        currentSegment.handleIn += event.delta;
        currentSegment.handleOut.angle = currentSegment.handleIn.angle+180;
        //currentSegment.handleOut.length = currentSegment.handleIn.length;
      } else if (hitResult.type === "handle-out") {
        currentSegment.handleIn.angle = currentSegment.handleOut.angle+180;
        //currentSegment.handleIn.length = currentSegment.handleOut.length;
        currentSegment.handleOut += event.delta;
      } else if (hitResult.type === "segment"){
        currentSegment.point = event.point;
      }
    }
  }
  else {

  }
}

function createInitHandles() {
  var fp = new Point(path.firstSegment.point);
  var lp = new Point(path.lastSegment.point);
  var vector = lp - fp;
  var handleVector = new Point();

  handleVector.length = 15;
  handleVector.angle = vector.angle + 180;
  path.firstSegment.handleOut = handleVector;
  path.lastSegment.handleIn = handleVector;
  handleVector.angle = vector.angle;
  path.firstSegment.handleIn = handleVector;
  path.lastSegment.handleOut = handleVector;

  isInitHandlesCreated = true;
}



