
var resetButton, smoothButton, modeButton; 
var mode = "path";

window.addEventListener("load", init, false);

function init() {
  //Global variables init
  resetButton = document.querySelector("#resetButton");
  smoothButton = document.querySelector("#smoothButton");
  modeButton = document.querySelector("#modeButton");


  //Event listeners
  resetButton.addEventListener("click", pResetClick, false );
  smoothButton.addEventListener("click", pSmoothClick, false );
  modeButton.addEventListener("click", pModeClick, false );
}

function pResetClick(evt) {
  //paper.project.activeLayer.children["pathCurve"].removeSegments();
  paper.project.activeLayer.removeChildren();
  paper.view.draw();
  isInitHandlesCreated = false;

}

function pSmoothClick(evt) {
  paper.project.activeLayer.children["pathCurve"].smooth();
  paper.view.draw();

}

function pModeClick(evt) {
  if (mode === "path") {
    modeButton.className = "checkedButton"
    mode = "cells";
  }
  else {
    modeButton.className = "normalButton";
    mode = "path";
  }

}
